Tests that scripts panel UI elements work as intended.


Running: testInitialLoad
mock-target-1
  top
    (no domain)
      bar.js
      baz.js
      foo.js
      foobar.js

Running: testReset
mock-target-2
  top
    (no domain)
      bar.js
      baz.js
      foo.js
Revealing in navigator.
mock-target-2
  top
    (no domain)
      bar.js
      baz.js
      foo.js
mock-target-2
  top
    (no domain)
      bar.js

Running: testDebuggerUISourceCodeAddedAndRemoved
mock-target-3
  top
    (no domain)
      foo.js
mock-target-3
  top
    (no domain)
      foo.js
      source.js

