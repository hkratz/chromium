Tests the Timeline API function call record for InjectedScript.eval call feature.

FunctionCall Properties:
{
    data : {
        frame : <string>
        scriptId : <string>
        scriptLine : <number>
        scriptName : <string>
        stackTrace : <object>
    }
    endTime : <number>
    frameId : <string>
    stackTrace : <object>
    startTime : <number>
    type : "FunctionCall"
}
Text details for FunctionCall: undefined

