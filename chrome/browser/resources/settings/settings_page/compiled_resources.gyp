# Copyright 2015 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
{
  'targets': [
    {
      'target_name': 'settings_page_visibility',
      'includes': ['../../../../../third_party/closure_compiler/compile_js.gypi'],
    },
  ],
}
